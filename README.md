# UnityPhonebook

Phonebook app made in Unity3D

# Releases
Releases of the app can be found in: APKs/* directory.

| Version       | Description|
| ------------- |:-------------:|
| 1.0.0      | All default requirements, create, update, delete, search, dynamic spawning of views, unit/integration tests|
<i>The idea is to make multiple releases. Version 1.0.0 will have all the default requirements of the phonebook app.
Releases following this will have improvements upon the design or implementation like for example maybe; SQL, object pooling, ECS
My reasoning is that I want to be able to deploy when needed, and any improvements I may be able to make are bonus. Also, this fits in nicely with agile development.</i> 

# Usage

This project uses submodules, to initialize them do the following steps:

- Clone this repository 
- Go to the repository in your terminal
- Type `git submodule init' in your terminal.
- Type `git submodule update' in your terminal.

# Submodules

I included a submodule in this project that I am writing myself, in private. It is a case study project for a test-driven Model View Controller (MVC) architecture in Unity3D. It is far from done and still in an experimental phase, for example: Currently I am not using any `models' in the project. This is to experiment with the Unity-Testing module to see how unit/integration testing(stability) holds while writing them againts MonoBehaviours. 
However, the package does provide me with some useful utilities already, like the controller architecture, view spawning and persistence.

# Continuous Integration

I added gitlab-ci files to be able to run all testcases in the gitlab-ci, it uses free gitlab runners with a Unity3D Docker image to build and run the tests.
See https://github.com/GabLeRoux/unity3d-ci-example for more information on unity gitlab-ci.
<i>I notice that the pipeline for RunTime-Tests will not complete. I am not sure why this keeps happening.</i>

# Some comments, on comments
The assignment gives hints that the code should be commented correctly. As you may find, there are not many comments in the sourcecode.
The fact is I do not like comments in code, they often lie, are wrong, and do not get updated together with the code they try to describe.
I however, try to make my code readable and descriptive and try to express the intent of the code simply through syntax.

# Model View Controller architecture & implementation

The app is build on top of my MVC framework.
The framework uses an Object called Main to startup and inject all dependencies needed.
It is build as a Clean/plugin/hexagonal architecture.
A plugin (currently) is of type `ScriptableObject` to support editor-time configuration. (Time will tell if I actually keep it this way) 
The framework has an MVC plugin that will generate instances of all controllers found in the assembly.
This means, that there will only be 1 controller for multiple views of the same type.
The controller will subscribe to default events thrown by the view (e.g. started, enabled, destroyed), but you need to implement an assignevents method to catch custom events of you views.
At the start of a scene, all views are queried and assigned to their controllers.
When a new view is spawned by a controller, the controller will assign itself to the newly spawned view.
All spawnable views must be added to a scriptableobject called ViewFactory.

The framework also has a default implementation for Json persistence. (all repositories are queried in a similar manner as the controllers, there are factories for this) It is implemented with a repository pattern that uses objects called gateways to write-out the data (a gateway could also be implemented with http for example).
Each repository should provide functions for all queries needed, this will help with dependency management, for example; We do not want to write literal sql strings in out source code, but do this only in the data layer. (yet for now, there is only Json)

# Example Controller implementation
For the first feature I implemented I added a `ContactsPageController`.
This controller will show the listing of all contacts and supports logic to catch the event by the add button.
When the `add contact event` is caught, it will show the addcontactview, then the `AddContactController` will take over the functionality to handle all logic for creating the new contact.
Also, the ContactsPageController implements a RepositoryListener interface, to listen for updates to the contacts repository.
When contacts are added, deleted or updated the controller can update the view.

# Repositories
The repositories will provide all functions need to get the models for memory, or disk. Listeners can be added to catch events from the repositories.
The ContactRepository implements 2 private listeners that will make sure when contacts are added, deleted or updated the sorting is done.

# Usecases
Since I do not like controllers with large and many functions I employ these objects called `Usecases`
The usecase objects are created and started by controllers and fully embed the functionality to complete the usecase (for example: AddContact).
Another large advantage for using the usecases, is that testing them is far more easier than Monobehaviours since they (should!,) are independent of the Unity event loop (awake, start, update).

# Services
The MVC package also has objects called services, they are singleton objects and can be retrieved through the ServiceProvider.GetService<T>.
This app has (so far) 1 service; The SortingService. This service keeps track of the current SortingMode (Alphabetical, Chronological).
I used a service for this since the sortingmode is a global variable, and I think this is a bit cleaner and easier to test than a static variable for example.
There are also 2 native services, the UpdateService and the CoroutineService but they are not used in this project at this moment.   

# Testing
Tests reside in the directories called `Specs` they are separated in Run-time tests and Editor-time tests.

# Vendor / Third-party Libraries
| Library       | Purpose           | Source |
| ------------- |:-------------:| -----:|
| DoTween      | For linear / algorithmic animation tweening| Unity3D asset store|
| NewtonsoftJson      | JSON (de)serializing|   https://www.newtonsoft.com/json |