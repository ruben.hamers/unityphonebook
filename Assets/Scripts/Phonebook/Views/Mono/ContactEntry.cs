using System;
using HamerSoft.Core;
using UnityEngine;
using UnityEngine.UI;
using UnityPhoneBook.Extensions;
using Object = HamerSoft.Core.Object;

namespace UnityPhoneBook.View.Mono
{
    /// <summary>
    /// Contact entry object (this represents a row in the contacts page)
    /// For now, lets not make it a view since it is part of the contacts page, maybe later?
    /// </summary>
    [RequireComponent(typeof(Button))]
    public class ContactEntry : Object
    {
        [SerializeField] private Text Label, Name;
        private Button _button;
        public string PhoneNumber { get; protected set; }
        private Action<int,string> _pressedCallback;

        protected override void Awake()
        {
            base.Awake();
            _button = GetComponent<Button>();
            _button.AddListener(ShowContextMenu);
            ValidateComponents();
        }

        private void ValidateComponents()
        {
            if (!Application.isEditor)
                return;
            Label.Verify("Label", this);
            Name.Verify("Name", this);
        }

        private void ShowContextMenu()
        {
            _pressedCallback?.Invoke(transform.GetSiblingIndex(), PhoneNumber);
        }

        public void Init(string phoneNumber, string firstName, string lastName, Action<int, string> pressedCallback)
        {
            _pressedCallback = pressedCallback;
            PhoneNumber = phoneNumber;
            if (Label && firstName.Length > 0)
                Label.text = firstName[0].ToString();
            if (Name)
                Name.text = $"{firstName} {lastName}";
        }
    }
}