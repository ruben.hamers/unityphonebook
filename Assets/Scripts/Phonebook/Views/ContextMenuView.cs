using DG.Tweening;
using HamerSoft.Core;
using HamerSoft.Core.Services;
using HamerSoft.UnityMvc;
using UnityEngine;
using UnityEngine.UI;
using UnityPhoneBook.Extensions;
using UnityPhoneBook.Services;

namespace UnityPhoneBook.Views
{
    public class ContextMenuView : BaseView
    {
        public event ViewEvent DetailsPressed, DeletePressed;
        [SerializeField] private Button DetailsButton, DeleteButton;
        private RectTransform _rectTransform;
        private Tweener _tweener;
        private string _currentContactId;
        private int _index;
        private const int ExpandedHeight = 150;
        private ContactsPageView _contactsPageView;

        protected override object[] CustomStartArgs { get; }

        protected override void Awake()
        {
            base.Awake();
            _rectTransform = transform as RectTransform;
            DetailsButton.AddListener(() => { DetailsPressed?.Invoke(this, _currentContactId, _contactsPageView); });
            DeleteButton.AddListener(DeleteContact);
            gameObject.SetActive(false);
            VerifyView();
            SortingService.SortingChanged += SortingServiceOnSortingChanged;
        }

        private void SortingServiceOnSortingChanged(SortingMode obj)
        {
            ForceCollapse();
        }

        protected override void OnDestroy()
        {
            _tweener?.Kill();
            SortingService.SortingChanged -= SortingServiceOnSortingChanged;
            base.OnDestroy();
        }

        private void DeleteContact()
        {
            DeletePressed?.Invoke(this, _currentContactId);
            ForceCollapse();
        }

        private void ForceCollapse()
        {
            _currentContactId = null;
            _index = 0;
            gameObject.SetActive(false);
            _rectTransform.sizeDelta = new Vector2(_rectTransform.sizeDelta.x, 0);
        }

        private void VerifyView()
        {
            if (!Application.isEditor)
                return;
            DeleteButton.Verify("Delete Button", this);
            DetailsButton.Verify("Details Button", this);
        }

        public void SetIndex(int index, string contactId)
        {
            if (_currentContactId == contactId)
                Collapse();
            else
                Expand(index, contactId);
        }

        private void Collapse()
        {
            _tweener?.Kill();
            if (gameObject.activeInHierarchy)
                _tweener = _rectTransform.DOSizeDelta(new Vector2(_rectTransform.sizeDelta.x, 0), .5f)
                    .OnComplete(() =>
                    {
                        _tweener = null;
                        gameObject.SetActive(false);
                    });
            else
            {
                gameObject.SetActive(true);
                _rectTransform.DOSizeDelta(new Vector2(_rectTransform.sizeDelta.x, ExpandedHeight), .5f)
                    .OnComplete(() => { _tweener = null; });
            }
        }

        private void Expand(int index, string contactId)
        {
            _tweener?.Kill();
            _index = SetTransformIndex(index);
            _currentContactId = contactId;
            gameObject.SetActive(true);
            _rectTransform.sizeDelta = new Vector2(_rectTransform.sizeDelta.x, 0);
            _tweener = _rectTransform.DOSizeDelta(new Vector2(_rectTransform.sizeDelta.x, ExpandedHeight), .5f)
                .OnComplete(() => { _tweener = null; });
        }

        private int SetTransformIndex(int index)
        {
            if (index <= _index || _index == 0)
                transform.SetSiblingIndex(++index);
            else
                transform.SetSiblingIndex(index);
            return index;
        }

        public void SetContactsPageView(ContactsPageView contactsView)
        {
            _contactsPageView = contactsView;
        }
    }
}