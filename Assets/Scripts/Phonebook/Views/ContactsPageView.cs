using System.Collections.Generic;
using System.Linq;
using HamerSoft.Core;
using HamerSoft.UnityMvc;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityPhoneBook.View.Mono;

namespace UnityPhoneBook.Views
{
    /// <summary>
    /// View representing the Contacts page.
    /// This View will support Searching, adding and deleting of contacts.
    /// Also, a scrollable view will be shown to iterate over all known contacts
    /// </summary>
    public class ContactsPageView : PageView
    {
        /// <summary>
        /// custom view events
        /// </summary>
        public event ViewEvent SearchChanged, ContactAddPressed, ContactEntryPressed, SortingChanged;

        [SerializeField] private ContactEntry EntryPrefab;
        private InputField _searchBar;
        private LayoutGroup _layoutGroup;
        private Button _addContactButton;
        private Dropdown _sortingDropdown;
        private Dictionary<string, ContactEntry> _contacts;
        private int _currentSorting;
        private string _currentSearch;
        protected override object[] CustomStartArgs => new[] {GetComponentInChildren<VerticalLayoutGroup>()};

        protected override void Awake()
        {
            base.Awake();
            _contacts = new Dictionary<string, ContactEntry>();
            _searchBar = GetComponentInChildren<InputField>();
            _layoutGroup = GetComponentInChildren<VerticalLayoutGroup>();
            _addContactButton = GetComponentInChildren<Button>();
            _sortingDropdown = GetComponentInChildren<Dropdown>();
            AssignSearchBar();
            AssignAddContactButton();
        }

        /// <summary>
        /// assigns the contactaddpressed event to the unity button component on click listener.
        /// </summary>
        private void AssignAddContactButton()
        {
            _addContactButton.AddListener(() => { ContactAddPressed?.Invoke(this, new object[0]); });
        }

        /// <summary>
        /// assigns the SearchChanged event to the unity inputfield value changed listener.
        /// We will have to see if this is ok, or we need to use the OnEndEdit event.
        /// </summary>
        private void AssignSearchBar()
        {
            if (_searchBar)
                _searchBar.onValueChanged.AddListener(Search);
        }

        private void Search(string query)
        {
            _currentSearch = query;
            SearchChanged?.Invoke(this, query);
        }

        // todo  add pooling in the future! but first get all requirements in
        public void SpawnContact(string phoneNumber, string firstName, string lastName)
        {
            var contactEntry = Instantiate(EntryPrefab, _layoutGroup.transform);
            contactEntry.Init(phoneNumber, firstName, lastName, EntryPressed);
            _contacts.Add(phoneNumber, contactEntry);
        }

        // todo add pooling in the future! but first get all requirements in
        public void SpawnContactInOrder(int index, string phoneNumber, string firstName, string lastName)
        {
            var contactEntry = Instantiate(EntryPrefab, _layoutGroup.transform);
            contactEntry.Init(phoneNumber, firstName, lastName, EntryPressed);
            contactEntry.transform.SetSiblingIndex(index);
            _contacts.Add(phoneNumber, contactEntry);
        }

        private void EntryPressed(int index, string phoneNumber)
        {
            ContactEntryPressed?.Invoke(this, index, phoneNumber);
        }

        // todo add pooling in the future! but first get all requirements in
        public void RemoveAllEntries()
        {
            _contacts = new Dictionary<string, ContactEntry>();
            if (!_layoutGroup)
                return;
            var entries = _layoutGroup.GetComponentsInChildren<ContactEntry>();
            // ReSharper disable once ForCanBeConvertedToForeach DO NOT CONVERT TO FOREACH SINCE THE COLLECTION WILL BE MODIFIED THROUGH THE DESTROY CALL!
            for (int i = 0; i < entries.Length; i++)
                Destroy(entries[i].gameObject);
        }

        public void DestroyContact(string removedContact)
        {
            if (!_contacts.ContainsKey(removedContact))
                return;
            var entry = _contacts[removedContact];
            _contacts.Remove(removedContact);
            Destroy(entry.gameObject);
        }

        public void SetupSortingDropdown(List<string> options, int currentSorting)
        {
            if (!_sortingDropdown || options == null)
                return;
            _sortingDropdown.onValueChanged.RemoveListener(SortingDropdownChanged);
            var dropdownOptions = new List<Dropdown.OptionData>();
            options.ForEach(o => dropdownOptions.Add(new Dropdown.OptionData(o)));
            _sortingDropdown.options = dropdownOptions;
            _currentSorting = _sortingDropdown.value = currentSorting;
            _sortingDropdown.onValueChanged.AddListener(SortingDropdownChanged);
        }

        private void SortingDropdownChanged(int newSorting)
        {
            if (_currentSorting == newSorting)
                return;
            _currentSorting = newSorting;
            SortingChanged?.Invoke(this, _sortingDropdown.options[newSorting].text, _currentSearch);
        }
    }
}