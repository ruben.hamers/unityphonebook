using HamerSoft.UnityMvc;
using UnityEngine;

namespace UnityPhoneBook.Views
{
    public abstract class PageView : BaseView
    {
        protected void Verify(Object target, string fieldName)
        {
            if (!target)
                Debug.LogWarning($"{fieldName} on {name} is null!");
        }

    }
}