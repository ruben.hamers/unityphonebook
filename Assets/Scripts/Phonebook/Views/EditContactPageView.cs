using HamerSoft.Core;
using HamerSoft.UnityMvc;
using UnityEngine;
using UnityEngine.UI;
using UnityPhoneBook.Extensions;

namespace UnityPhoneBook.Views
{
    public class EditContactPageView : PageView
    {
        public event ViewEvent CancelPressed, SavePressed;

        [SerializeField] private Text Title, Notifications;
        [SerializeField] private InputField PhoneNumber, FirstName, LastName, Description, Email, TwitterHandle;
        [SerializeField] private Button CancelButton, SaveButton;
        protected override object[] CustomStartArgs { get; }
        private string _oldPhoneNumber;

        protected override void Awake()
        {
            base.Awake();
            VerifyView();
            CancelButton.AddListener(() => { CancelPressed?.Invoke(this, new object[1] {_oldPhoneNumber}); });
            SaveButton.AddListener(SaveContact);
        }

        public override void Init(params object[] initArguments)
        {
            base.Init(initArguments);
            _oldPhoneNumber = initArguments.GetArgument<string>(0);
            string firstName = initArguments.GetArgument<string>(1);
            string lastName = initArguments.GetArgument<string>(2);
            string description = initArguments.GetArgument<string>(3);
            string email = initArguments.GetArgument<string>(4);
            string twitter = initArguments.GetArgument<string>(5);
            if (Title)
                Title.text = $"{firstName} {lastName}";
            SetInputField(PhoneNumber, _oldPhoneNumber);
            SetInputField(FirstName, firstName);
            SetInputField(LastName, lastName);
            SetInputField(Description, description);
            SetInputField(Email, email);
            SetInputField(TwitterHandle, twitter);
        }

        private void SetInputField(InputField field, string text)
        {
            if (!string.IsNullOrEmpty(text))
                field.SetTextSafe(text);
        }

        private void SaveContact()
        {
            SavePressed?.Invoke(this,
                _oldPhoneNumber,
                PhoneNumber.GetTextSafe(),
                FirstName.GetTextSafe(),
                LastName.GetTextSafe(),
                Description.GetTextSafe(),
                Email.GetTextSafe(),
                TwitterHandle.GetTextSafe());
        }

        public void ShowNotification(string notification)
        {
            if (Notifications)
                Notifications.text = notification;
        }

        private void VerifyView()
        {
            if (!Application.isEditor)
                return;

            Verify(Title, "Title");
            Verify(Notifications, "Notifications");
            Verify(PhoneNumber, "PhoneNumber");
            Verify(FirstName, "FirstName");
            Verify(LastName, "LastName");
            Verify(Description, "Description");
            Verify(Email, "Email");
            Verify(TwitterHandle, "TwitterHandle");
            Verify(CancelButton, "CancelButton");
            Verify(SaveButton, "SaveButton");
        }
    }
}