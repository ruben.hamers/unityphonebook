using HamerSoft.Core;
using HamerSoft.UnityMvc;
using UnityEngine;
using UnityEngine.UI;

namespace UnityPhoneBook.Views
{
    public class DetailsPageView : PageView
    {
        public event ViewEvent EditPressed, DeletePressed, BackPressed;

        [SerializeField] private Text Title, PhoneNumber, FirstName, LastName, Description, Email, TwitterHandle;
        [SerializeField] private Button EditButton, DeleteButton, BackButton;
        protected override object[] CustomStartArgs { get; }

        protected override void Awake()
        {
            base.Awake();
            VerifyView();
            EditButton.AddListener(() => { EditPressed?.Invoke(this, PhoneNumber.text); });
            DeleteButton.AddListener(() => { DeletePressed?.Invoke(this, PhoneNumber.text); });
            BackButton.AddListener((() => { BackPressed?.Invoke(this, new object[0]); }));
        }

        public override void Init(params object[] initArguments)
        {
            base.Init(initArguments);
            string phoneNumber = initArguments.GetArgument<string>(0);
            string firstName = initArguments.GetArgument<string>(1);
            string lastName = initArguments.GetArgument<string>(2);
            string description = initArguments.GetArgument<string>(3);
            string email = initArguments.GetArgument<string>(4);
            string twitter = initArguments.GetArgument<string>(5);
            if (Title)
                Title.text = $"{firstName} {lastName}";
            SetText(PhoneNumber, phoneNumber);
            SetText(FirstName, firstName);
            SetText(LastName, lastName);
            SetText(Description, description);
            SetText(Email, email);
            SetText(TwitterHandle, twitter);
        }

        private void SetText(Text field, string text)
        {
            if (!string.IsNullOrEmpty(text) && field)
                field.text = text;
        }
        
        private void VerifyView()
        {
            if (!Application.isEditor)
                return;
            Verify(Title, "Title");
            Verify(PhoneNumber, "PhoneNumber");
            Verify(FirstName, "FirstName");
            Verify(LastName, "LastName");
            Verify(Description, "Description");
            Verify(Email, "Email");
            Verify(TwitterHandle, "TwitterHandle");
            Verify(EditButton, "EditButton");
            Verify(DeleteButton, "DeleteButton");
            Verify(BackButton, "BackButton");
        }
    }
}