using HamerSoft.Core;
using HamerSoft.UnityMvc;
using UnityEngine;
using UnityEngine.UI;
using UnityPhoneBook;
using UnityPhoneBook.Extensions;

namespace UnityPhoneBook.Views
{
    public class AddContactView : PageView
    {
        public event ViewEvent Cancelled, Saved;
        [SerializeField] private InputField PhoneNumber, FirstName, LastName, Description, Email, Twitter;
        [SerializeField] private Text Notifications;
        [SerializeField] private Button CancelButton, SaveButton;
        protected override object[] CustomStartArgs { get; }

        protected override void Awake()
        {
            base.Awake();
            VerifyFields();
            AssignCancelButton();
            AssignSaveButton();
        }

        private void AssignCancelButton()
        {
            CancelButton.AddListener(() => { Cancelled?.Invoke(this, new object[0]); });
        }

        private void AssignSaveButton()
        {
            SaveButton.AddListener(SaveContact);
        }

        private void SaveContact()
        {
            Saved?.Invoke(this,
                PhoneNumber.GetTextSafe(),
                FirstName.GetTextSafe(),
                LastName.GetTextSafe(),
                Description.GetTextSafe(),
                Email.GetTextSafe(),
                Twitter.GetTextSafe());
        }

        private void VerifyFields()
        {
            if (!Application.isEditor)
                return;
            Verify(PhoneNumber, "phone number");
            Verify(FirstName, "first name");
            Verify(LastName, "last name");
            Verify(Description, "description");
            Verify(Email, "email");
            Verify(Twitter, "twitter");
            Verify(CancelButton, "Cancel button");
            Verify(SaveButton, "Add button");
            Verify(Notifications, "Notifications field");
        }

        public void ShowNotification(string notification)
        {
            if (Notifications)
                Notifications.text = notification;
        }
    }
}