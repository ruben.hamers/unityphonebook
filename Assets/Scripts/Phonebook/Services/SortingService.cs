using System;
using HamerSoft.Core.Services;

namespace UnityPhoneBook.Services
{
    /// <summary>
    /// sorting mode enum
    /// increment by 10 so we can add multiple Alphabetically sorted modes in the future (difference between descending, ascending maybe?)
    /// </summary>
    public enum SortingMode
    {
        Alphabetically = 0,
        Chronologically = 10
    }

    /// <summary>
    /// Use a service since they are implemented as singletons that can be retrieved through ServiceProvider.Get<T>
    /// Services are instantiated when Main is Init
    /// I want it to be static since the sorting is done through the entire game `session'
    /// </summary>
    public class SortingService : IService
    {
        /// <summary>
        /// use static event to always be able to assign events
        /// Event is only fired when the mode changes!
        /// </summary>
        public static event Action<SortingMode> SortingChanged;
        
        public SortingMode SortingMode { get; protected set; }

        public SortingService()
        {
            SortingMode = SortingMode.Alphabetically;
        }

        public void SetSorting(SortingMode mode)
        {
            if (SortingMode == mode)
                return;
            SortingMode = mode;
            SortingChanged?.Invoke(SortingMode);
        }

        public void Dispose()
        {
        }
    }
}