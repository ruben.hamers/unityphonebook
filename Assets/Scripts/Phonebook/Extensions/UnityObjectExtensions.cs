using UnityEngine;

namespace UnityPhoneBook.Extensions
{
    public static class UnityObjectExtensions
    {
        public static void Verify(this Object target, string fieldName, HamerSoft.Core.Object component)
        {
            if (!target)
                Debug.LogWarning($"{fieldName} on {(component? component.name: "destroyed component")} is null!");
        }
    }
}