using UnityEngine.UI;

namespace UnityPhoneBook.Extensions
{
    public static class InputFieldExtensions
    {
        /// <summary>
        /// get text safely from inputfield
        /// </summary>
        /// <param name="field">target inputfield</param>
        /// <returns>the field.text or null when the inputfield is null</returns>
        public static string GetTextSafe(this InputField field)
        {
            return field ? field.text : null;
        }

        /// <summary>
        /// set text safely in inputfield
        /// </summary>
        /// <param name="field">target inputfield</param>
        public static void SetTextSafe(this InputField field, string text)
        {
            if (field)
                field.text = text;
        }
    }
}