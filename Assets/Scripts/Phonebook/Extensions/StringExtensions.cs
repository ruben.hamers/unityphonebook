using System.Text.RegularExpressions;

namespace UnityPhoneBook.Extensions
{
    public static class StringExtensions
    {
        private static readonly string phoneNumberRegex = @"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$";

        private static readonly string emailRegex =
            @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";

        /// <summary>
        /// check for a valid phone number
        /// </summary>
        /// <param name="phoneNumber">the phone number string</param>
        /// <returns>true when matches the regex</returns>
        public static bool IsValidPhoneNumber(this string phoneNumber)
        {
            return !string.IsNullOrEmpty(phoneNumber) && Regex.IsMatch(phoneNumber, phoneNumberRegex);
        }

        /// <summary>
        /// check if the string is a valid email address
        /// </summary>
        /// <param name="email">the email string</param>
        /// <returns>true when it matches the email regex</returns>
        public static bool IsValidEmail(this string email)
        {
            return !string.IsNullOrEmpty(email) && Regex.IsMatch(email, emailRegex, RegexOptions.IgnoreCase);
        }
    }
}