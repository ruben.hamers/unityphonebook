using System;
using HamerSoft.Core;
using Newtonsoft.Json;

namespace UnityPhoneBook.Models
{
    /// <summary>
    /// contact model
    /// </summary>
    public class Contact : IIdentifiable
    {
        /// <summary>
        /// format used for datetime string formatting
        /// </summary>
        private const string DateFormat = "yyyyMMddHHmmss.fff";

        /// <summary>
        /// Id is unique, for simplicity, we use the phone number as the unique key
        /// We can use this to make sure no duplicate numbers are added to the contacts
        /// </summary>
        [JsonProperty("Id")]
        public string Id { get; private set; }

        [JsonProperty("PhoneNumber")] public string PhoneNumber { get; private set; }
        [JsonProperty("FirstName")] public string FirstName { get; private set; }
        [JsonProperty("LastName")] public string LastName { get; private set; }
        [JsonProperty("Description")] public string Description { get; private set; }
        [JsonProperty("Email")] public string Email { get; private set; }
        [JsonProperty("TwitterHandle")] public string TwitterHandle { get; private set; }
        [JsonProperty("CreatedAt")] public string CreatedAt { get; private set; }
        [JsonProperty("QueryString")] public string QueryString { get; private set; }

        public Contact(string phoneNumber, string firstName, string lastName)
        {
            Id = phoneNumber;
            CreatedAt = DateTime.Now.ToString(DateFormat);
            PhoneNumber = phoneNumber;
            FirstName = firstName;
            LastName = lastName;
            SetQueryString();
        }

        private void SetQueryString()
        {
            QueryString = $"{FirstName} {LastName}";
        }

        public void SetPhoneNumber(string phoneNumber)
        {
            Id = phoneNumber;
            PhoneNumber = phoneNumber;
        }

        public void SetFirstName(string firstName)
        {
            FirstName = firstName;
            SetQueryString();
        }

        public void SetLastName(string lastName)
        {
            LastName = lastName;
            SetQueryString();
        }

        public void SetDescription(string description)
        {
            Description = description;
        }

        public void SetEmail(string email)
        {
            Email = email;
        }

        public void SetTwitterHandle(string twitterHandle)
        {
            if (string.IsNullOrEmpty(twitterHandle))
                TwitterHandle = twitterHandle;
            else
                TwitterHandle = !twitterHandle.StartsWith("@") ? $"@{twitterHandle}" : twitterHandle;
        }

        public void Dispose()
        {
            // no interesting dispose pattern yet
        }

        /// <summary>
        /// simple contains function based on the query string
        /// </summary>
        /// <param name="query">query</param>
        /// <returns>true if the QueryString contains the query</returns>
        public bool Contains(string query)
        {
            return string.IsNullOrEmpty(query) || QueryString.Contains(query);
        }
    }
}