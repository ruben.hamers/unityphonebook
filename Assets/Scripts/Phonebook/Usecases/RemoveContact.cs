using System;
using HamerSoft.Core.UseCases;
using UnityPhoneBook.Repositories;

namespace UnityPhoneBook.Usecases
{
    public class RemoveContact : UseCase<bool>
    {
        private readonly string _phoneNumber;
        private readonly ContactRepository _contactRepository;

        public RemoveContact(string phoneNumber, ContactRepository contactRepository)
        {
            _phoneNumber = phoneNumber;
            _contactRepository = contactRepository;
        }

        public override void Execute(Action<bool> callback)
        {
            base.Execute(callback);
            var contact = _contactRepository.GetObject(_phoneNumber);
            if (contact != null)
            {
                _contactRepository.RemoveObject(ref contact);
                _contactRepository.Flush();
                InvokeCallback(true);
            }
            else
            {
                InvokeCallback(false);
            }
        }

        protected override void DisposeUseCase()
        {
        }
    }
}