using System;
using HamerSoft.Core.UseCases;
using UnityPhoneBook.Extensions;
using UnityPhoneBook.Models;
using UnityPhoneBook.Repositories;

namespace UnityPhoneBook.Usecases
{
    /// <summary>
    /// add contact usecase object
    /// simple object for now, but might be more complex in the future.
    /// This handles input validation for phonenumbers and email since we do not want to rely on the frontend
    /// Note: no checks for sql injection of any sort since we do not support sql yet, maybe later!
    /// </summary>
    public class AddContact : UseCase<Contact, string>
    {
        private readonly ContactRepository _repository;
        private readonly string _phoneNumber;
        private readonly string _firstName;
        private readonly string _lastName;
        private string _description;
        private string _email;
        private string _twitterHandle;

        public AddContact(ContactRepository repository, string phoneNumber, string firstName, string lastName)
        {
            _repository = repository;
            _phoneNumber = phoneNumber;
            _firstName = firstName;
            _lastName = lastName;
        }

        public void AddDescription(string description)
        {
            _description = description;
        }

        public void AddEmail(string email)
        {
            _email = email?.ToLower();
        }

        public void AddTwitterHandle(string twitterHandle)
        {
            _twitterHandle = twitterHandle;
        }

        public override void Execute(Action<Contact, string> callback)
        {
            base.Execute(callback);
            if (_repository == null)
                ShowNoRepositoryFound();
            else if (string.IsNullOrEmpty(_phoneNumber) || string.IsNullOrEmpty(_firstName) ||
                     string.IsNullOrEmpty(_lastName))
                ShowRequiredFieldsAreEmpty();
            else if(!_phoneNumber.IsValidPhoneNumber())
                ShowInvalidPhoneNumber();
            else if (_repository.HasObject(_phoneNumber))
                ShowPhoneNumberExists();
            else if(!string.IsNullOrEmpty(_email) && !_email.IsValidEmail())
                ShowEmailInvalid();
            else
            {
                var contact = CreateContact();
                _repository.AddObject(contact);
                _repository.Flush();
                InvokeCallback(contact, "Successfully added new contact!");
            }
        }

        private void ShowEmailInvalid()
        {
            InvokeCallback(null, "The entered email is invalid!");
        }

        private void ShowPhoneNumberExists()
        {
            InvokeCallback(null, $"A contact with the phone number {_phoneNumber} already exists!");
        }

        private void ShowInvalidPhoneNumber()
        {
            InvokeCallback(null, "The entered phone number is invalid!");
        }

        private void ShowRequiredFieldsAreEmpty()
        {
            InvokeCallback(null, "Phone number, first and last name are required fields!");
        }

        private void ShowNoRepositoryFound()
        {
            InvokeCallback(null, "No database found, please contact an admin if this problem persists!");
        }

        private Contact CreateContact()
        {
            var contact = new Contact(_phoneNumber, _firstName, _lastName);
            if (!string.IsNullOrEmpty(_description))
                contact.SetDescription(_description);
            if (!string.IsNullOrEmpty(_email))
                contact.SetEmail(_email);
            if (!string.IsNullOrEmpty(_twitterHandle))
                contact.SetTwitterHandle(_twitterHandle);
            return contact;
        }

        protected override void DisposeUseCase()
        {
        }
    }
}