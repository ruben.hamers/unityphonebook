using System;
using HamerSoft.Core.UseCases;
using UnityPhoneBook.Extensions;
using UnityPhoneBook.Models;
using UnityPhoneBook.Repositories;

namespace UnityPhoneBook.Usecases
{
    public class EditContact : UseCase<Contact, string>
    {
        private readonly ContactRepository _repository;
        private readonly string _oldPhoneNumber;
        private readonly string _newPhoneNumber;
        private readonly string _firstName;
        private readonly string _lastName;
        private readonly Contact _existingContact;
        private string _description;
        private string _email;
        private string _twitterHandle;

        public EditContact(ContactRepository repository, string oldPhoneNumber, string newPhoneNumber, string firstName,
            string lastName)
        {
            _repository = repository;
            _existingContact = _repository?.GetObject(oldPhoneNumber);
            _oldPhoneNumber = oldPhoneNumber;
            _newPhoneNumber = newPhoneNumber;
            _firstName = firstName;
            _lastName = lastName;
        }

        public void AddDescription(string description)
        {
            _description = description;
        }

        public void AddEmail(string email)
        {
            _email = email?.ToLower();
        }

        public void AddTwitterHandle(string twitterHandle)
        {
            _twitterHandle = twitterHandle;
        }

        public override void Execute(Action<Contact, string> callback)
        {
            base.Execute(callback);
            if (_repository == null)
                ShowNoRepositoryFound();
            else if (string.IsNullOrEmpty(_newPhoneNumber) || string.IsNullOrEmpty(_firstName) ||
                     string.IsNullOrEmpty(_lastName))
                ShowRequiredFieldsAreEmpty();
            else if (_oldPhoneNumber != _newPhoneNumber && !_newPhoneNumber.IsValidPhoneNumber())
                ShowInvalidPhoneNumber();
            else if (_oldPhoneNumber != _newPhoneNumber && _repository.HasObject(_newPhoneNumber))
                ShowPhoneNumberExists();
            else if (!string.IsNullOrEmpty(_email) && !_email.IsValidEmail())
                ShowEmailInvalid();
            else
            {
                _repository.UpdateObject(UpdateContact());
                _repository.Flush();
                InvokeCallback(_existingContact, "Successfully added new contact!");
            }
        }

        private void ShowEmailInvalid()
        {
            InvokeCallback(null, "The entered email is invalid!");
        }

        private void ShowPhoneNumberExists()
        {
            InvokeCallback(null, $"A contact with the phone number {_newPhoneNumber} already exists!");
        }

        private void ShowInvalidPhoneNumber()
        {
            InvokeCallback(null, "The entered phone number is invalid!");
        }

        private void ShowRequiredFieldsAreEmpty()
        {
            InvokeCallback(null, "Phone number, first and last name are required fields!");
        }

        private void ShowNoRepositoryFound()
        {
            InvokeCallback(null, "No database found, please contact an admin if this problem persists!");
        }

        private Contact UpdateContact()
        {
            _existingContact.SetPhoneNumber(_newPhoneNumber);
            _existingContact.SetFirstName(_firstName);
            _existingContact.SetLastName(_lastName);
            _existingContact.SetDescription(_description);
            _existingContact.SetEmail(_email);
            _existingContact.SetTwitterHandle(_twitterHandle);
            return _existingContact;
        }

        protected override void DisposeUseCase()
        {
        }
    }
}