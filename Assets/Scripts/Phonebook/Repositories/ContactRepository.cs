using System.Collections.Generic;
using System.Linq;
using HamerSoft.Core;
using UnityPhoneBook.Models;
using UnityPhoneBook.Repositories.Gateways;
using UnityPhoneBook.Services;

namespace UnityPhoneBook.Repositories
{
    public class ContactRepository : Repository<Contact>
    {
        private ContactsChronological _contactsChronological;
        private ContactsAlphabetical _contactsAlphabetically;

        public ContactRepository()
        {
            var allContacts = GetAll();
            _contactsAlphabetically = new ContactsAlphabetical(allContacts);
            _contactsChronological = new ContactsChronological(allContacts);
            AddListener(_contactsAlphabetically);
            AddListener(_contactsChronological);
        }

        protected override IGateway<Contact> CreateGateway()
        {
            return new ContactGateway();
        }

        public List<Contact> GetAllBySortMode(SortingMode sortingMode)
        {
            switch (sortingMode)
            {
                case SortingMode.Alphabetically:
                    return GetAllAlphabetical();
                case SortingMode.Chronologically:
                    return GetAllChronological();
                default:
                    return GetAllAlphabetical();
            }
        }

        /// <summary>
        /// get all contacts in alphabetical order
        /// </summary>
        /// <returns>contacts sorted by firstname, then lastname</returns>
        public List<Contact> GetAllAlphabetical()
        {
            return _contactsAlphabetically.GetContacts();
        }

        /// <summary>
        /// get all contacts by date
        /// </summary>
        /// <returns>contacts sorted by date added</returns>
        public List<Contact> GetAllChronological()
        {
            return _contactsChronological.GetContacts();
        }

        /// <summary>
        /// get the sorted index by mode
        /// </summary>
        /// <param name="added">the contact object</param>
        /// <param name="sortingMode">sorting mode</param>
        /// <returns>index in correct sorting</returns>
        public int GetSortIndexBySortMode(Contact added, SortingMode sortingMode)
        {
            switch (sortingMode)
            {
                case SortingMode.Alphabetically:
                    return GetSortIndexAlphabetical(added);
                case SortingMode.Chronologically:
                    return GetSortIndexChronological(added);
            }

            return 0;
        }

        /// <summary>
        /// get the index of the contact based on alphabetical sorting
        /// </summary>
        /// <param name="contact">the contact</param>
        /// <returns>the index in the list</returns>
        public int GetSortIndexAlphabetical(Contact contact)
        {
            return _contactsAlphabetically.GetContacts().IndexOf(contact);
        }

        /// <summary>
        /// get the sort index of the contact based on chronological sorting
        /// </summary>
        /// <param name="contact">the contact</param>
        /// <returns>index in the list</returns>
        public int GetSortIndexChronological(Contact contact)
        {
            return _contactsChronological.GetContacts().IndexOf(contact);
        }

        /// <summary>
        /// get all contacts containing the query string, when the query is empty, just return all
        /// Todo maybe add a nice full text search engine like Lucene in the future! 
        /// </summary>
        /// <param name="query">query string</param>
        /// <param name="sortingMode">soring mode to use</param>
        /// <returns>all contacts containing the string</returns>
        public List<Contact> GetContactsContaining(string query, SortingMode sortingMode)
        {
            switch (sortingMode)
            {
                case SortingMode.Alphabetically:
                    return string.IsNullOrEmpty(query)
                        ? _contactsAlphabetically.GetContacts()
                        : _contactsAlphabetically.GetContacts().Where(c => c.Contains(query)).ToList();
                case SortingMode.Chronologically:
                    return string.IsNullOrEmpty(query)
                        ? _contactsChronological.GetContacts()
                        : _contactsChronological.GetContacts().Where(c => c.Contains(query)).ToList();
                default:
                    return string.IsNullOrEmpty(query)
                        ? _contactsChronological.GetContacts()
                        : _contactsChronological.GetContacts().Where(c => c.Contains(query)).ToList();
            }
        }

        /// <summary>
        /// internal listener to do sorting
        /// we do this since we do not want to sort all contacts each time we call the GetAllAlphabetical or GetAllByDate
        /// </summary>
        private abstract class ContactSortingListener : IRepositoryListener<Contact>
        {
            protected SortedList<string, Contact> SortedList;

            protected ContactSortingListener(List<Contact> contacts)
            {
                SortedList = new SortedList<string, Contact>();
                contacts.ForEach(AddedObject);
            }

            protected abstract string GetKey(Contact contact);

            public virtual void AddedObject(Contact added)
            {
                string key = GetKey(added);
                if (!SortedList.ContainsKey(key))
                    SortedList.Add(key, added);
            }

            public virtual void RemovedObject(Contact removed)
            {
                string key = GetKey(removed);
                if (SortedList.ContainsKey(key))
                    SortedList.Remove(removed.CreatedAt);
            }

            public virtual void UpdatedObject(Contact updated)
            {
                string key = GetKey(updated);
                if (SortedList.ContainsKey(key))
                    SortedList[key] = updated;
            }

            public List<Contact> GetContacts()
            {
                return SortedList.Values.ToList();
            }
        }
        /// <summary>
        /// sort contacts alphabetical listener
        /// </summary>
        private class ContactsAlphabetical : ContactSortingListener
        {
            public ContactsAlphabetical(List<Contact> contacts) : base(contacts)
            {
            }

            protected override string GetKey(Contact contact)
            {
                return $"{contact.FirstName} {contact.LastName}";
            }
        }

        /// <summary>
        /// sort contacts by date listener
        /// </summary>
        private class ContactsChronological : ContactSortingListener
        {
            public ContactsChronological(List<Contact> contacts) : base(contacts)
            {
            }

            protected override string GetKey(Contact contact)
            {
                return contact.CreatedAt;
            }
        }
    }
}