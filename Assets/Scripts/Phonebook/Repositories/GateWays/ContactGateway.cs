using HamerSoft.UnityJsonPersistence;
using UnityPhoneBook.Models;

namespace UnityPhoneBook.Repositories.Gateways
{
    public class ContactGateway : JsonGateway<Contact>
    {
        protected override string SubDirectory => "Entities/Contacts";
    }
}