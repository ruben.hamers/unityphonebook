using System.Collections.Generic;
using System.Linq;
using HamerSoft.Core;
using HamerSoft.Core.Services;
using HamerSoft.UnityMvc;
using Newtonsoft.Json.Utilities;
using UnityPhoneBook.Views;
using UnityEngine.UI;
using UnityPhoneBook.Models;
using UnityPhoneBook.Repositories;
using UnityPhoneBook.Services;
using Canvas = HamerSoft.Core.Singletons.Canvas;

namespace UnityPhoneBook
{
    /// <summary>
    /// controller for the contacts page
    /// This controller will handle viewing all known contacts, searching and providing of a view to add a new contact
    /// </summary>
    public class ContactsPageController : BaseController<ContactsPageView>, IRepositoryListener<Contact>
    {
        private ContactRepository _contactRepository;
        private ContextMenuView _contextMenu;
        private static readonly Dictionary<string, SortingMode> SortingModes = new Dictionary<string, SortingMode>
        {
            {"A,B..Z", SortingMode.Alphabetically},
            {"Date", SortingMode.Chronologically},
        };

        public override void SetMain(Main main)
        {
            base.SetMain(main);
            AddRepositoryListener(main);
        }

        /// <summary>
        /// add this controller as listener to the repository to receive notifications when contacts are added, removed and updated
        /// </summary>
        /// <param name="main">main</param>
        private void AddRepositoryListener(Main main)
        {
            if (!main)
                return;

            _contactRepository = main.GetRepository<ContactRepository>();
            _contactRepository.AddListener(this);
        }

        /// <summary>
        /// assign custom events
        /// </summary>
        /// <param name="view">target view</param>
        protected override void AssignViewEvents(ContactsPageView view)
        {
            view.SearchChanged += ViewOnSearchChanged;
            view.ContactAddPressed += ViewOnContactAddPressed;
            view.ContactEntryPressed += ViewOnContactEntryPressed;
            view.SortingChanged += ViewOnSortingChanged;
        }

        /// <summary>
        /// when the view is destroyed, remove the events to avoid memory leaks and nullreference exceptions
        /// </summary>
        /// <param name="view">a contactspageview</param>
        /// <param name="args">arguments</param>
        protected override void OnDestroyed(HamerSoft.UnityMvc.View view, object[] args)
        {
            base.OnDestroyed(view, args);
            var cpv = CastTo(view);
            cpv.SearchChanged -= ViewOnSearchChanged;
            cpv.ContactAddPressed -= ViewOnContactAddPressed;
            cpv.ContactEntryPressed -= ViewOnContactEntryPressed;
        }

        protected override void OnStarted(HamerSoft.UnityMvc.View view, object[] args)
        {
            base.OnStarted(view, args);
            var layoutGroup = args.GetArgument<VerticalLayoutGroup>(0);
            var contactsView = CastTo(view);
            _contactRepository.GetAllBySortMode(ServiceProvider.GetService<SortingService>().SortingMode)
                .ForEach(c => contactsView.SpawnContact(c.PhoneNumber, c.FirstName, c.LastName));
            _contextMenu = SpawnCustomView<ContextMenuView>(layoutGroup.transform);
            _contextMenu.SetContactsPageView(contactsView);
            contactsView.SetupSortingDropdown(SortingModes.Keys.ToList(), SortingModes.IndexOf(kvp =>kvp.Value ==ServiceProvider.GetService<SortingService>().SortingMode));
        }

        /// <summary>
        /// process the fired event from the view when the inputfield has been changed
        /// </summary>
        /// <param name="view">a contactspage view </param>
        /// <param name="outputArguments">output arguments, this will include the text of the input field</param>
        private void ViewOnSearchChanged(HamerSoft.UnityMvc.View view, object[] outputArguments)
        {
            string currentSearch = outputArguments.GetArgument<string>(0);
            var contactsView = CastTo(view);
            contactsView.RemoveAllEntries();
            _contactRepository.GetContactsContaining(currentSearch, ServiceProvider.GetService<SortingService>().SortingMode)
                .ForEach(c => contactsView.SpawnContact(c.PhoneNumber, c.FirstName, c.LastName));
        }

        /// <summary>
        /// process the fired event from the view when the addcontact event is fired
        /// </summary>
        /// <param name="view">a contacts page vew</param>
        /// <param name="outputArguments">arguments, this will be an empty array for now</param>
        private void ViewOnContactAddPressed(HamerSoft.UnityMvc.View view, object[] outputArguments)
        {
            DestroyView(CastTo(view));
            SpawnCustomView<AddContactView>(Canvas.GetInstance().transform);
        }

        private void ViewOnContactEntryPressed(HamerSoft.UnityMvc.View view, object[] outputArguments)
        {
            int index = outputArguments.GetArgument<int>(0);
            string phoneNumber = outputArguments.GetArgument<string>(1);
            _contextMenu.SetIndex(index, phoneNumber);
        }

        private void ViewOnSortingChanged(HamerSoft.UnityMvc.View view, object[] outputArguments)
        {
            string sortmode = outputArguments.GetArgument<string>(0);
            string currentSearch = outputArguments.GetArgument<string>(1);
            if(string.IsNullOrEmpty(sortmode) || !SortingModes.ContainsKey(sortmode))
                return;

            SortingMode newMode = SortingModes[sortmode];
            ServiceProvider.GetService<SortingService>().SetSorting(newMode);
            var contactsView = CastTo(view);
            contactsView.RemoveAllEntries();
            _contactRepository.GetContactsContaining(currentSearch, newMode)
                .ForEach(c => contactsView.SpawnContact(c.PhoneNumber, c.FirstName, c.LastName));
            _contextMenu.transform.SetAsLastSibling();
        }

        /// <summary>
        /// A contact has been added to the repository
        /// </summary>
        /// <param name="added">the added contact</param>
        public void AddedObject(Contact added)
        {
            if (!Main)
                return;
            foreach (KeyValuePair<string, ContactsPageView> view in Views)
                if (IsValid(view.Value))
                    view.Value.SpawnContactInOrder(_contactRepository.GetSortIndexBySortMode(added,ServiceProvider.GetService<SortingService>().SortingMode),
                        added.PhoneNumber,
                        added.FirstName, added.LastName);
        }

        public void RemovedObject(Contact removed)
        {
            if (!Main)
                return;
            foreach (KeyValuePair<string, ContactsPageView> view in Views)
                if (IsValid(view.Value))
                    view.Value.DestroyContact(removed.Id);
        }

        public void UpdatedObject(Contact updated)
        {
            // no implementation needed, this view is never active during an edit
        }

        public override void Dispose()
        {
            base.Dispose();
            _contactRepository?.RemoveListener(this);
        }
    }
}