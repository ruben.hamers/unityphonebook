using HamerSoft.Core;
using HamerSoft.Core.Singletons;
using HamerSoft.UnityMvc;
using UnityPhoneBook.Usecases;
using UnityPhoneBook.Repositories;
using UnityPhoneBook.Views;

namespace UnityPhoneBook.Controllers
{
    public class EditContactController : BaseController<EditContactPageView>
    {
        private ContactRepository _contactRepository;

        public override void SetMain(Main main)
        {
            base.SetMain(main);
            if (main)
                _contactRepository = main.GetRepository<ContactRepository>();
        }

        protected override void AssignViewEvents(EditContactPageView view)
        {
            view.CancelPressed += ViewOnCancelPressed;
            view.SavePressed += ViewOnSavePressed;
        }

        private void ViewOnCancelPressed(HamerSoft.UnityMvc.View view, object[] outputArguments)
        {
            string oldPhoneNumber = outputArguments.GetArgument<string>(0);
            var contact = _contactRepository.GetObject(oldPhoneNumber);
            DestroyView(CastTo(view));
            if (contact != null)
                SpawnCustomView<DetailsPageView>(Canvas.GetInstance().transform, null, contact.Id,
                    contact.FirstName, contact.LastName, contact.Description, contact.Email, contact.TwitterHandle);
            else
                SpawnCustomView<ContactsPageView>(Canvas.GetInstance().transform);
        }

        private void ViewOnSavePressed(HamerSoft.UnityMvc.View view, object[] outputArguments)
        {
            string oldPhoneNumber = outputArguments.GetArgument<string>(0);
            string newPhoneNumber = outputArguments.GetArgument<string>(1);
            string firstName = outputArguments.GetArgument<string>(2);
            string lastName = outputArguments.GetArgument<string>(3);
            string description = outputArguments.GetArgument<string>(4);
            string email = outputArguments.GetArgument<string>(5);
            string twitter = outputArguments.GetArgument<string>(6);
            var editContact = new EditContact(_contactRepository, oldPhoneNumber, newPhoneNumber, firstName, lastName);
            editContact.AddDescription(description);
            editContact.AddEmail(email);
            editContact.AddTwitterHandle(twitter);
            editContact.Execute((contact, notification) =>
            {
                if (contact == null)
                {
                    CastTo(view).ShowNotification(notification);
                }
                else
                {
                    DestroyView(CastTo(view));
                    SpawnCustomView<DetailsPageView>(Canvas.GetInstance().transform, null, contact.Id,
                        contact.FirstName, contact.LastName, contact.Description, contact.Email, contact.TwitterHandle);
                }
            });
        }
    }
}