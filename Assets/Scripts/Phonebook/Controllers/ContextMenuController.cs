using HamerSoft.Core;
using HamerSoft.Core.Singletons;
using HamerSoft.UnityMvc;
using UnityPhoneBook.Usecases;
using UnityPhoneBook.Repositories;
using UnityPhoneBook.Views;

namespace UnityPhoneBook.Controllers
{
    public class ContextMenuController : BaseController<ContextMenuView>
    {
        private ContactRepository _contactRepository;

        protected override void AssignViewEvents(ContextMenuView view)
        {
            view.DeletePressed += ViewOnDeletePressed;
            view.DetailsPressed += ViewOnDetailsPressed;
        }

        protected override void OnDestroyed(HamerSoft.UnityMvc.View view, object[] args)
        {
            base.OnDestroyed(view, args);
            var contextMenu = CastTo(view);
            contextMenu.DeletePressed -= ViewOnDeletePressed;
            contextMenu.DetailsPressed -= ViewOnDetailsPressed;
        }

        public override void SetMain(Main main)
        {
            base.SetMain(main);
            if (main)
                _contactRepository = main.GetRepository<ContactRepository>();
        }

        private void ViewOnDetailsPressed(HamerSoft.UnityMvc.View view, object[] outputArguments)
        {
            string phoneNumber = outputArguments.GetArgument<string>(0);
            var contactsPage = outputArguments.GetArgument<ContactsPageView>(1);
            var contact = _contactRepository.GetObject(phoneNumber);
            if (contact != null)
            {
                if(contactsPage)
                contactsPage.ForceDestroyObject();
                DestroyView(CastTo(view));
                SpawnCustomView<DetailsPageView>(Canvas.GetInstance().transform, null, contact.Id,
                    contact.FirstName, contact.LastName, contact.Description, contact.Email, contact.TwitterHandle);
            }
        }

        private void ViewOnDeletePressed(HamerSoft.UnityMvc.View view, object[] outputArguments)
        {
            string phoneNumber = outputArguments.GetArgument<string>(0);
            new RemoveContact(phoneNumber, _contactRepository).Execute(success =>
            {
                // no callback implementation yet!
            });
        }
    }
}