using HamerSoft.Core;
using HamerSoft.Core.Singletons;
using HamerSoft.UnityMvc;
using UnityPhoneBook.Usecases;
using UnityPhoneBook.Repositories;
using UnityPhoneBook.Views;

namespace UnityPhoneBook.Controllers
{
    public class DetailsPageController : BaseController<DetailsPageView>
    {
        private ContactRepository _contactRepository;

        public override void SetMain(Main main)
        {
            base.SetMain(main);
            if (main)
                _contactRepository = main.GetRepository<ContactRepository>();
        }

        protected override void AssignViewEvents(DetailsPageView view)
        {
            view.EditPressed += ViewOnEditPressed;
            view.DeletePressed += ViewOnDeletePressed;
            view.BackPressed += ViewOnBackPressed;
        }

        protected override void OnDestroyed(HamerSoft.UnityMvc.View view, object[] args)
        {
            base.OnDestroyed(view, args);
            var detailsPage = CastTo(view);
            detailsPage.EditPressed -= ViewOnEditPressed;
            detailsPage.DeletePressed -= ViewOnDeletePressed;
            detailsPage.BackPressed -= ViewOnBackPressed;
        }

        private void ViewOnBackPressed(HamerSoft.UnityMvc.View view, object[] outputArguments)
        {
            DestroyView(CastTo(view));
            SpawnCustomView<ContactsPageView>(Canvas.GetInstance().transform);
        }

        private void ViewOnDeletePressed(HamerSoft.UnityMvc.View view, object[] outputArguments)
        {
            string phoneNumber = outputArguments.GetArgument<string>(0);

            new RemoveContact(phoneNumber, _contactRepository).Execute(success =>
            {
                DestroyView(CastTo(view));
                SpawnCustomView<ContactsPageView>(Canvas.GetInstance().transform);
            });
        }

        private void ViewOnEditPressed(HamerSoft.UnityMvc.View view, object[] outputArguments)
        {
            string phoneNumber = outputArguments.GetArgument<string>(0);
            var contact = _contactRepository.GetObject(phoneNumber);
            if (contact != null)
            {
                DestroyView(CastTo(view));
                SpawnCustomView<EditContactPageView>(Canvas.GetInstance().transform, null, contact.Id,
                    contact.FirstName, contact.LastName, contact.Description, contact.Email, contact.TwitterHandle);
            }
        }
    }
}