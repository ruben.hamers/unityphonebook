using HamerSoft.Core;
using HamerSoft.Core.Singletons;
using HamerSoft.UnityMvc;
using UnityPhoneBook.Usecases;
using UnityPhoneBook.Views;
using UnityPhoneBook.Repositories;

namespace UnityPhoneBook
{
    public class AddContactController : BaseController<AddContactView>
    {
        private ContactRepository _contactRepository;

        public override void SetMain(Main main)
        {
            base.SetMain(main);
            _contactRepository = Main.GetRepository<ContactRepository>();
        }

        protected override void AssignViewEvents(AddContactView view)
        {
            view.Cancelled += ViewOnCancelled;
            view.Saved += ViewOnSaved;
        }

        protected override void OnDestroyed(HamerSoft.UnityMvc.View view, object[] args)
        {
            base.OnDestroyed(view, args);
            var addContact = CastTo(view);
            addContact.Cancelled += ViewOnCancelled;
            addContact.Saved += ViewOnSaved;
        }

        private void ViewOnCancelled(HamerSoft.UnityMvc.View view, object[] outputArguments)
        {
            DestroyView(CastTo(view));
            SpawnCustomView<ContactsPageView>(Canvas.GetInstance().transform);
        }

        private void ViewOnSaved(HamerSoft.UnityMvc.View view, object[] outputArguments)
        {
            string phoneNumber = outputArguments.GetArgument<string>(0);
            string firstName = outputArguments.GetArgument<string>(1);
            string lastName = outputArguments.GetArgument<string>(2);
            string description = outputArguments.GetArgument<string>(3);
            string email = outputArguments.GetArgument<string>(4);
            string twitter = outputArguments.GetArgument<string>(5);
            var addContact = new AddContact(_contactRepository, phoneNumber, firstName, lastName);
            addContact.AddDescription(description);
            addContact.AddEmail(email);
            addContact.AddTwitterHandle(twitter);
            addContact.Execute((contact, notification) =>
            {
                if (contact == null)
                {
                    CastTo(view).ShowNotification(notification);
                }
                else
                {
                    DestroyView(CastTo(view));
                    SpawnCustomView<ContactsPageView>(Canvas.GetInstance().transform);
                }
            });
        }
    }
}