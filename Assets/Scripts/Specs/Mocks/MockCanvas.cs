using System.Reflection;
using HamerSoft.Core.Singletons;

namespace UnityPhoneBook.Specs.Mocks
{
    public class MockCanvas : HamerSoft.Core.Singletons.Canvas
    {
        /// <summary>
        /// Override the awake to force a new instance to be set
        /// Use reflection to access the field, since I do not want to expose this field to anyone!
        /// This will automatically be called when running runtime-tetss
        /// </summary>
        protected override void Awake()
        {
            //base.Awake();
            typeof(AbstractSingleton<Canvas>).GetField("_instance", BindingFlags.Static | BindingFlags.NonPublic)
                .SetValue(this, this);
        }

        /// <summary>
        /// When running editor-tests use this to reset the instance of the canvas singleton.
        /// </summary>
        public void CallAwake()
        {
            Awake();
        }
    }
}