using System;
using System.Linq;
using System.Text;
using NUnit.Framework;
using UnityEditor;
using UnityPhoneBook.Models;
using UnityPhoneBook.Repositories;
using UnityPhoneBook.Services;
using Random = UnityEngine.Random;

namespace UnityPhoneBook.Specs.RepositoryTests
{
    
    [TestFixture]
    public class ContactRepositoryTests
    {
        private ContactRepository _repository;
        [SetUp]
        public void Setup()
        {
            _repository = new ContactRepository();
        }

        [MenuItem("UnityPhoneBook/Generate test contacts")]
        public static void GenerateTestContacts()
        {
            var repo = new ContactRepository();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 50; i++)
            {
                for (int j = 0; j < 10; j++)
                    sb.Append(Random.Range(0, 10));
                var contact = new Contact(sb.ToString(),$"firstname{i}",$"lastname{i}");
                repo.AddObject(contact);
                sb.Clear();
            }    
            repo.Flush();
        }
        
        
        [Test]
        public void When_Contact_Is_Added_It_Is_Retrievable_Alphabetically()
        {
            var contact = new Contact("FooBar", "Foo", "Bar");
            _repository.AddObject(contact);
            Assert.NotNull(_repository.GetAllAlphabetical().FirstOrDefault(c=>c.Id == contact.Id));
            _repository.RemoveObject(ref contact);
        }
        [Test]
        public void When_Contact_Is_Added_Its_Index_Is_Retrievable_Alphabetically()
        {
            //force the contact to be added as last by setting name to ZZZ
            var contact = new Contact("FooBar", "ZZZZZZZZZZZZZZZZZZZZZZZZZZZ", "ZZZZZZZZZZZZZZZZZZZZZZZZZZZ");
            _repository.AddObject(contact);
            Assert.AreEqual(_repository.GetAllAlphabetical().Count-1,_repository.GetSortIndexAlphabetical(contact));
            _repository.RemoveObject(ref contact);
        }
        
        [Test]
        public void When_Contact_Is_Added_Its_Index_Is_Retrievable_Alphabetically_In_Correct_Order()
        {
            var contact1 = new Contact("FooBar", "ZZZZZZZZZZZZZZZZZZZZZZZZZA", "ZZZZZZZZZZZZZZZZZZZZZZZZZZA");
            var contact2 = new Contact("BazQux", "ZZZZZZZZZZZZZZZZZZZZZZZZZZZ", "ZZZZZZZZZZZZZZZZZZZZZZZZZZZ");
            _repository.AddObject(contact1);
            _repository.AddObject(contact2);
            Assert.Less(_repository.GetSortIndexAlphabetical(contact1),_repository.GetSortIndexAlphabetical(contact2));
            _repository.RemoveObject(ref contact1);
            _repository.RemoveObject(ref contact2);
        }
        
        [Test]
        public void When_Contact_Is_Added_It_Is_Retrievable_Chronologically()
        {
            var contact = new Contact("FooBar", "Foo", "Bar");
            _repository.AddObject(contact);
            Assert.NotNull(_repository.GetAllChronological().FirstOrDefault(c=>c.Id == contact.Id));
            _repository.RemoveObject(ref contact);
        }
        [Test]
        public void When_Contact_Is_Added_Its_Index_Is_Retrievable_Chronologically()
        {
            var contact = new Contact("FooBar", "Foo", "Bar");
            _repository.AddObject(contact);
            Assert.AreEqual(_repository.GetAllChronological().Count-1,_repository.GetSortIndexChronological(contact));
            _repository.RemoveObject(ref contact);
        }
        
        [Test]
        public void When_Contact_Is_Added_Its_Index_Is_Retrievable_Chronologically_In_Correct_Order()
        {
            var contact1 = new Contact("FooBar", "Foo", "Bar");
            var contact2 = new Contact("BazQux", "Baz", "Qux");
            _repository.AddObject(contact1);
            _repository.AddObject(contact2);
            Assert.Less(_repository.GetSortIndexChronological(contact1),_repository.GetSortIndexChronological(contact2));
            _repository.RemoveObject(ref contact1);
            _repository.RemoveObject(ref contact2);
        }
        
        [Test]
        public void When_Search_Query_Is_Empty_All_Contacts_Are_Returned()
        {
            var contact1 = new Contact("FooBar", "Foo", "Bar");
            var contact2 = new Contact("BazQux", "Baz", "Qux");
            _repository.AddObject(contact1);
            _repository.AddObject(contact2);
            Assert.AreEqual(_repository.GetAllAlphabetical(),_repository.GetContactsContaining(String.Empty, SortingMode.Alphabetically));
            _repository.RemoveObject(ref contact1);
            _repository.RemoveObject(ref contact2);
        }
        
        [Test]
        public void When_Contact_Contains_The_Query_It_Is_Returned()
        {
            var contact1 = new Contact("FooBar", "When_Contact_Contains_The_Query_It_Is_Returned", "Bar");
            _repository.AddObject(contact1);
            Assert.NotNull(_repository.GetContactsContaining("When_Contact_Contains_The_Query_It_Is_Returned", SortingMode.Alphabetically));
            _repository.RemoveObject(ref contact1);
        }
        
        [Test]
        public void When_Multiple_Contacts_Contain_The_Query_They_Are_Returned()
        {
            var contact1 = new Contact("FooBar", "Foo", "When_Multiple_Contacts_Contain_The_Query_They_Are_Returned");
            var contact2 = new Contact("BazQux", "When_Multiple_Contacts_Contain_The_Query_They_Are_Returned", "Qux");
            _repository.AddObject(contact1);
            _repository.AddObject(contact2);
            Assert.AreEqual(2,_repository.GetContactsContaining("When_Multiple_Contacts_Contain_The_Query_They_Are_Returned", SortingMode.Alphabetically).Count);
            _repository.RemoveObject(ref contact1);
            _repository.RemoveObject(ref contact2);
        }
    }
}