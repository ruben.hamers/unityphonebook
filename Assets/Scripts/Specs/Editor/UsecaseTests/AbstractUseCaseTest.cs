using HamerSoft.Core;
using NUnit.Framework;
using UnityEngine;
using UnityPhoneBook.Specs.Mocks;

namespace UnityPhoneBook.Specs.UseCaseTests
{
    [TestFixture]
    public class AbstractUseCaseTest
    {
        protected Main Main;
        protected MockCanvas Canvas;

        [SetUp]
        public virtual void Setup()
        {
            GetMain();
            Canvas = new GameObject("Root").AddComponent<MockCanvas>();
        }

        /// <summary>
        /// Get main from resources
        /// </summary>
        protected void GetMain()
        {
            Main = Resources.Load<Main>("Scriptables/HamerSoftMain");
            Main.Init();
        }

        [TearDown]
        public virtual void TearDown()
        {
        }
    }
}