using NUnit.Framework;
using UnityPhoneBook.Usecases;
using UnityPhoneBook.Models;
using UnityPhoneBook.Repositories;

namespace UnityPhoneBook.Specs.UseCaseTests
{
    public class EditContactTests : AbstractUseCaseTest
    {
        private ContactRepository _contactRepository;
        private Contact _existingContact;
        private const string OldPhoneNumber ="5050505050";

        public override void Setup()
        {
            base.Setup();
            _contactRepository = Main.GetRepository<ContactRepository>();
            _existingContact = new Contact(OldPhoneNumber, "Editcontact usecase","tests");
            _contactRepository.AddObject(_existingContact);
        }

        [Test]
        public void When_EditContact_Is_Executed_Without_PhoneNumber_It_Returns_Null_With_A_Notification()
        {
            new EditContact(_contactRepository,OldPhoneNumber, null, "Foo", "Bar").Execute((contact, notification) =>
            {
                Assert.Null(contact);
                Assert.NotNull(notification);
            });
        }

        [Test]
        public void When_EditContact_Is_Executed_Without_FirstName_It_Returns_Null_With_A_Notification()
        {
            new EditContact(_contactRepository,OldPhoneNumber, "Foo", null, "Bar").Execute((contact, notification) =>
            {
                Assert.Null(contact);
                Assert.NotNull(notification);
            });
        }

        [Test]
        public void When_EditContact_Is_Executed_Without_LastName_It_Returns_Null_With_A_Notification()
        {
            new EditContact(_contactRepository, OldPhoneNumber, "Foo", "Bar", null).Execute((contact, notification) =>
            {
                Assert.Null(contact);
                Assert.NotNull(notification);
            });
        }

        [Test]
        public void When_EditContact_Is_Executed_With_Invalid_PhoneNumber_It_Returns_Null_With_A_Notification()
        {
            new EditContact(_contactRepository, OldPhoneNumber,"Foo", "Bar", "Baz").Execute((contact, notification) =>
            {
                Assert.Null(contact);
                Assert.NotNull(notification);
            });
        }

        [Test]
        public void When_EditContact_Is_Executed_With_Invalid_Email_It_Returns_Null_With_A_Notification()
        {
            var edit = new EditContact(_contactRepository, OldPhoneNumber,"Foo", "Bar", "Baz");
            edit.AddEmail("bla");
            edit.Execute((contact, notification) =>
            {
                Assert.Null(contact);
                Assert.NotNull(notification);
            });
        }
        
        [Test]
        public void When_EditContact_Is_Executed_With_A_New_But_Already_Existing_PhoneNumber_it_Returns_Null_With_A_Notification()
        {
           var stubContact = new Contact("1921921921","stubbed","contact");
           _contactRepository.AddObject(stubContact);
            var edit = new EditContact(_contactRepository, OldPhoneNumber,"1921921921", "Bar", "Baz");
            edit.Execute((contact, notification) =>
            {
                Assert.Null(contact);
                Assert.NotNull(notification);
                _contactRepository.RemoveObject(ref stubContact);
                _contactRepository.Flush();
            });
        }

        [Test]
        public void When_EditContact_Is_Executed_With_Valid_PhoneNumber_Names_But_No_Email_The_Contact_Is_Editted()
        {
            var edit = new EditContact(_contactRepository, OldPhoneNumber,"9191919191", "Bar", "Baz");
            edit.Execute((contact, notification) =>
            {
                Assert.NotNull(contact);
                _contactRepository.RemoveObject(ref contact);
                _contactRepository.Flush();
            });
        }

        [Test]
        public void When_EditContact_Is_Executed_With_Valid_PhoneNumber_Names_Email_The_Contact_Is_Created()
        {
            var edit = new EditContact(_contactRepository,OldPhoneNumber, "9191919191", "Bar", "Baz");
            edit.AddEmail("foo@bar.com");
            edit.Execute((contact, notification) =>
            {
                Assert.NotNull(contact);
                _contactRepository.RemoveObject(ref contact);
                _contactRepository.Flush();
            });
        }

        public override void TearDown()
        {
            base.TearDown();
            if (_existingContact != null)
            {
                _contactRepository.RemoveObject(ref _existingContact);
                _contactRepository.Flush();
            }
        }
    }
}