using NUnit.Framework;
using UnityPhoneBook.Usecases;
using UnityPhoneBook.Models;
using UnityPhoneBook.Repositories;

namespace UnityPhoneBook.Specs.UseCaseTests
{
    public class RemoveContactTests : AbstractUseCaseTest
    {
        private ContactRepository _contactRepository;
        private Contact _existingContact;
        private const string PhoneNumber ="3939393939";

        public override void Setup()
        {
            base.Setup();
            _contactRepository = Main.GetRepository<ContactRepository>();
            _existingContact = new Contact(PhoneNumber, "Removecontact usecase","tests");
            _contactRepository.AddObject(_existingContact);
        }

        [Test]
        public void When_Contact_Exists_It_Is_Removed()
        {
            new RemoveContact(PhoneNumber,_contactRepository).Execute(success =>
            {
                Assert.True(success);
                Assert.Null(_contactRepository.GetObject(PhoneNumber));
            });
        }
        
        [Test]
        public void When_Contact_Does_Not_Exist_It_The_UseCase_Returns_False()
        {
            new RemoveContact("When_Contact_Does_Not_Exist_It_The_UseCase_Returns_False",_contactRepository).Execute(Assert.False);
        }
        
        
        public override void TearDown()
        {
            base.TearDown();
            if (_existingContact != null)
            {
                _contactRepository.RemoveObject(ref _existingContact);
                _contactRepository.Flush();
            }
        }
    }
}