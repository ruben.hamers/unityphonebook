using NUnit.Framework;
using UnityPhoneBook.Usecases;
using UnityPhoneBook.Repositories;

namespace UnityPhoneBook.Specs.UseCaseTests
{
    [TestFixture]
    public class AddContactTests : AbstractUseCaseTest
    {
        private ContactRepository _contactRepository;

        public override void Setup()
        {
            base.Setup();
            _contactRepository = Main.GetRepository<ContactRepository>();
        }

        [Test]
        public void When_AddContact_Is_Executed_Without_PhoneNumber_It_Returns_Null_With_A_Notification()
        {
            new AddContact(_contactRepository, null, "Foo", "Bar").Execute((contact, notification) =>
            {
                Assert.Null(contact);
                Assert.NotNull(notification);
            });
        }

        [Test]
        public void When_AddContact_Is_Executed_Without_FirstName_It_Returns_Null_With_A_Notification()
        {
            new AddContact(_contactRepository, "Foo", null, "Bar").Execute((contact, notification) =>
            {
                Assert.Null(contact);
                Assert.NotNull(notification);
            });
        }

        [Test]
        public void When_AddContact_Is_Executed_Without_LastName_It_Returns_Null_With_A_Notification()
        {
            new AddContact(_contactRepository, "Foo", "Bar", null).Execute((contact, notification) =>
            {
                Assert.Null(contact);
                Assert.NotNull(notification);
            });
        }

        [Test]
        public void When_AddContact_Is_Executed_With_Invalid_PhoneNumber_It_Returns_Null_With_A_Notification()
        {
            new AddContact(_contactRepository, "Foo", "Bar", "Baz").Execute((contact, notification) =>
            {
                Assert.Null(contact);
                Assert.NotNull(notification);
            });
        }

        [Test]
        public void When_AddContact_Is_Executed_With_Invalid_Email_It_Returns_Null_With_A_Notification()
        {
            var add = new AddContact(_contactRepository, "Foo", "Bar", "Baz");
            add.AddEmail("bla");
            add.Execute((contact, notification) =>
            {
                Assert.Null(contact);
                Assert.NotNull(notification);
            });
        }

        [Test]
        public void When_AddContact_Is_Executed_With_Valid_PhoneNumber_Names_But_No_Email_The_Contact_Is_Created()
        {
            var add = new AddContact(_contactRepository, "9191919191", "Bar", "Baz");
            add.Execute((contact, notification) =>
            {
                Assert.NotNull(contact);
                _contactRepository.RemoveObject(ref contact);
                _contactRepository.Flush();
            });
        }

        [Test]
        public void When_AddContact_Is_Executed_With_Valid_PhoneNumber_Names_Email_The_Contact_Is_Created()
        {
            var add = new AddContact(_contactRepository, "9191919191", "Bar", "Baz");
            add.AddEmail("foo@bar.com");
            add.Execute((contact, notification) =>
            {
                Assert.NotNull(contact);
                _contactRepository.RemoveObject(ref contact);
                _contactRepository.Flush();
            });
        }
    }
}