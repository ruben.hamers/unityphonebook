using NUnit.Framework;
using UnityPhoneBook.Services;

namespace UnityPhoneBook.Specs.Services
{
    public class SortingServiceTests :AbstractServiceTests<SortingService>
    {
        protected override SortingService CreateService()
        {
            return new SortingService();
        }

        [Test]
        public void When_Sorting_Is_Updated_To_New_Sorting_The_SortingChanged_Event_Is_Fired()
        {
            void Sorted(SortingMode mode)
            {
                Assert.Pass(); 
            }

            SortingService.SortingChanged += Sorted;
            Service.SetSorting(SortingMode.Chronologically);
            SortingService.SortingChanged -= Sorted;
        }
        
        [Test]
        public void When_Sorting_Is_Updated_With_The_Same_Mode_The_SortingChanged_Event_Is_Not_Fired()
        {
            bool isFired = false;
            
            void Sorted(SortingMode mode)
            {
                isFired = true;
            }

            SortingService.SortingChanged += Sorted;
            Service.SetSorting(SortingMode.Alphabetically);
            Assert.False(isFired);
            SortingService.SortingChanged -= Sorted;
        }
    }
}