using HamerSoft.Core.Services;
using NUnit.Framework;

namespace UnityPhoneBook.Specs.Services
{
    public abstract class AbstractServiceTests<T> where T : IService
    {
        protected T Service;

        [SetUp]
        public virtual void Setup()
        {
            Service = CreateService();
        }

        protected abstract T CreateService();

        [TearDown]
        public virtual void TearDown()
        {
            Service?.Dispose();
        }
    }
}