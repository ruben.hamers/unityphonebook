using NUnit.Framework;
using UnityEngine;
using UnityEngine.UI;
using UnityPhoneBook.Extensions;

namespace UnityPhoneBook.Specs.ExtensionTests
{
    [TestFixture]
    public class ExtensionTests
    {
        [Test]
        public void InputField_GetTextSafe_Does_Not_Throw_An_Exception_When_The_InputField_Is_Null()
        {
            InputField field = null;
            Assert.DoesNotThrow(() => { field.GetTextSafe(); });
        }

        [Test]
        public void String_Validate_PhoneNumber_Is_Invalid_When_Null_Or_Empty()
        {
            string number = null;
            Assert.False(string.Empty.IsValidPhoneNumber());
            Assert.False(number.IsValidPhoneNumber());
        }

        [Test]
        public void String_Validate_PhoneNumber_Is_Invalid_When_Contains_Characters()
        {
            Assert.False("aaa".IsValidPhoneNumber());
        }

        [Test]
        public void String_Validate_PhoneNumber_Is_Invalid_When_To_Short()
        {
            Assert.False("1".IsValidPhoneNumber());
        }

        [Test]
        public void String_Validate_PhoneNumber_Is_Invalid_When_To_Long()
        {
            Assert.False("111111111111111111111111111".IsValidPhoneNumber());
        }

        [Test]
        public void String_Validate_PhoneNumber_Is_Valid_When_Has_10_Numeric_Characters()
        {
            Assert.True("0123456789".IsValidPhoneNumber());
        }

        [Test]
        public void String_Validate_Email_Is_Invalid_When_Null_Or_Empty()
        {
            string number = null;
            Assert.False(string.Empty.IsValidEmail());
            Assert.False(number.IsValidEmail());
        }

        [Test]
        public void String_Validate_Email_Is_Invalid_When_Not_Contains_AtSign()
        {
            Assert.False("Foo".IsValidEmail());
        }

        [Test]
        public void String_Validate_Email_Is_Invalid_When_Not_Contains_Dot_After_AtSign()
        {
            Assert.False("Foo@".IsValidEmail());
        }

        [Test]
        public void String_Validate_Email_Is_Invalid_When_Not_Contains_Characters_After_Dot_After_AtSign()
        {
            Assert.False("Foo@.".IsValidEmail());
        }

        [Test]
        public void String_Validate_Email_Is_Valid_When_Contains_Characters_After_Dot_After_AtSign()
        {
            Assert.True("foo@Bar.com".IsValidEmail());
        }
    }
}