﻿using System.Collections;
using System.Linq;
using System.Reflection;
using NUnit.Framework;
using UnityPhoneBook.Views;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.UI;
using UnityPhoneBook.Models;
using UnityPhoneBook.Repositories;
using UnityPhoneBook.Specs.Mocks;
using UnityPhoneBook.View.Mono;

namespace UnityPhoneBook.Specs
{
    public class ContactsPageControllerTests : AbstractControllerTests<ContactsPageController, ContactsPageView>
    {
        private Contact _contact1;
        private Contact _contact2;

        protected override ContactsPageView SetupView(MockCanvas canvas)
        {
            return Controller.SpawnView(Canvas.transform);
        }

        public override void Setup()
        {
            base.Setup();
            var repo = Main.GetRepository<ContactRepository>();
            _contact1 = repo.GetObject("contact1");
            if (_contact1 != null)
                repo.RemoveObject(ref _contact1);
            _contact2 = repo.GetObject("contact2");
            if (_contact2 != null)
                repo.RemoveObject(ref _contact2);
            repo.Flush();
        }

        /// <summary>
        /// Since this is the first test, some quick explanation why there are 2 waitforendofframe calls at the start and end of the test function
        /// I have noticed that these runtime-unity-tests tend to overlap in execution during testing.
        /// This means that tests do not run in isolation anymore.
        /// So, by adding these calls, we are sure that the previous test is finished.
        /// This is exceptionally noticeable when you are testing operations with callbacks that run asynchronously
        /// In this case, we will have to yield the test enumerator until the callback is invoked. (we will probably see some tests like that in the future)
        ///
        /// On the assert Catch:
        /// 
        /// We can not use Assert.Null() since the View is not null, yet is destroyed.
        /// This is an issue in Unity3D serialization 
        /// The View will be <null> as a serialized string!
        /// use a catch assertion to try and get a random value from the view, in this case its name
        /// this will throw an exception since the view is not valid anymore
        /// Set a breakpoint on the line: var test = View.name; to inspect what I mean.
        /// 
        /// </summary>
        /// <returns></returns>
        [UnityTest]
        public IEnumerator When_AddContacts_Is_Invoked_The_View_Will_Be_Destroyed()
        {
            yield return new WaitForEndOfFrame();
            InvokeControllerViewEventMethod("ViewOnContactAddPressed", new object[0]);
            yield return new WaitForEndOfFrame();
            Assert.Catch(() =>
            {
                var test = View.name;
            });
            yield return new WaitForSeconds(1);
        }

        [UnityTest]
        public IEnumerator When_AddContacts_Is_Invoked_AddContactView_Will_Be_Spawned()
        {
            yield return new WaitForEndOfFrame();
            InvokeControllerViewEventMethod("ViewOnContactAddPressed", new object[0]);
            yield return new WaitForEndOfFrame();
            Assert.NotNull(Canvas.GetComponentInChildren<AddContactView>());
            yield return new WaitForEndOfFrame();
        }

        [UnityTest]
        public IEnumerator When_Search_Is_Invoked_With_An_Empty_String_All_Contacts_Will_Be_Shown()
        {
            yield return new WaitForEndOfFrame();
            var contactRepo = Main.GetRepository<ContactRepository>();
            _contact1 = new Contact("contact1", "Foo", "Bar");
            _contact2 = new Contact("contact2", "Bar", "Qux");
            contactRepo.AddObject(_contact1);
            contactRepo.AddObject(_contact2);
            InvokeControllerViewEventMethod("ViewOnSearchChanged", new object[1] {string.Empty});
            yield return new WaitForEndOfFrame();
            Assert.NotNull(Canvas.GetComponentsInChildren<ContactEntry>()
                .FirstOrDefault(entry => entry.PhoneNumber == _contact1.Id));
            Assert.NotNull(Canvas.GetComponentsInChildren<ContactEntry>()
                .FirstOrDefault(entry => entry.PhoneNumber == _contact2.Id));
            contactRepo.RemoveObject(ref _contact1);
            contactRepo.RemoveObject(ref _contact2);
            contactRepo.Flush();
            yield return new WaitForEndOfFrame();
        }

        [UnityTest]
        public IEnumerator When_Search_Is_Invoked_With_A_String_That_Is_Contained_By_One_Contact_It_Will_Be_Shown()
        {
            yield return new WaitForEndOfFrame();
            var contactRepo = Main.GetRepository<ContactRepository>();
            _contact1 = new Contact("contact1", "Foo", "Bar");
            _contact2 = new Contact("contact2", "Bar", "Qux");
            contactRepo.AddObject(_contact1);
            contactRepo.AddObject(_contact2);
            InvokeControllerViewEventMethod("ViewOnSearchChanged", new object[1] {"Qux"});
            yield return new WaitForEndOfFrame();
            Assert.Null(Canvas.GetComponentsInChildren<ContactEntry>()
                .FirstOrDefault(entry => entry.PhoneNumber == _contact1.Id));
            Assert.NotNull(Canvas.GetComponentsInChildren<ContactEntry>()
                .FirstOrDefault(entry => entry.PhoneNumber == _contact2.Id));
            contactRepo.RemoveObject(ref _contact1);
            contactRepo.RemoveObject(ref _contact2);
            contactRepo.Flush();
            yield return new WaitForEndOfFrame();
        }

        [UnityTest]
        public IEnumerator When_Search_Is_Invoked_With_A_String_That_Is_Contained_By_Both_Contacts_They_Will_Be_Shown()
        {
            yield return new WaitForEndOfFrame();
            var contactRepo = Main.GetRepository<ContactRepository>();
            _contact1 = new Contact("contact1", "Foo", "Bar");
            _contact2 = new Contact("contact2", "Bar", "Qux");
            contactRepo.AddObject(_contact1);
            contactRepo.AddObject(_contact2);
            InvokeControllerViewEventMethod("ViewOnSearchChanged", new object[1] {"Bar"});
            yield return new WaitForEndOfFrame();
            Assert.NotNull(Canvas.GetComponentsInChildren<ContactEntry>()
                .FirstOrDefault(entry => entry.PhoneNumber == _contact1.Id));
            Assert.NotNull(Canvas.GetComponentsInChildren<ContactEntry>()
                .FirstOrDefault(entry => entry.PhoneNumber == _contact2.Id));
            contactRepo.RemoveObject(ref _contact1);
            contactRepo.RemoveObject(ref _contact2);
            contactRepo.Flush();
            yield return new WaitForEndOfFrame();
        }

        [UnityTest]
        public IEnumerator
            When_Search_Is_Invoked_With_A_String_That_Is_Not_Contained_By_Both_Contacts_They_Will_Not_Be_Shown()
        {
            yield return new WaitForEndOfFrame();
            var contactRepo = Main.GetRepository<ContactRepository>();
            _contact1 = new Contact("contact1", "Foo", "Bar");
            _contact2 = new Contact("contact2", "Bar", "Qux");
            contactRepo.AddObject(_contact1);
            contactRepo.AddObject(_contact2);
            InvokeControllerViewEventMethod("ViewOnSearchChanged",
                new object[1] {"!!This is a very long search query term that is used for testing purposes only!!"});
            yield return new WaitForEndOfFrame();
            Assert.Null(Canvas.GetComponentsInChildren<ContactEntry>()
                .FirstOrDefault(entry => entry.PhoneNumber == _contact1.Id));
            Assert.Null(Canvas.GetComponentsInChildren<ContactEntry>()
                .FirstOrDefault(entry => entry.PhoneNumber == _contact2.Id));
            contactRepo.RemoveObject(ref _contact1);
            contactRepo.RemoveObject(ref _contact2);
            contactRepo.Flush();
            yield return new WaitForEndOfFrame();
        }
        [UnityTest]
        public IEnumerator When_Started_The_ContextMenu_Is_Spawned()
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForSeconds(1);
            Assert.NotNull(typeof(ContactsPageController).GetField("_contextMenu", BindingFlags.Instance| BindingFlags.NonPublic).GetValue(Controller));
            yield return new WaitForEndOfFrame();
        }

        [UnityTest]
        public IEnumerator When_EntryPressed_Is_Invoked_The_Index_Is_Set_Correctly()
        {
            yield return new WaitForEndOfFrame();
            string phoneNumber = "!!Fake phonenumber!!";
            var layoutGroup = View.GetComponentInChildren<LayoutGroup>();
            for (int i = 0; i < 4; i++)
            {
                var stub = new GameObject();
                stub.transform.SetParent(layoutGroup.transform);
            }
            InvokeControllerViewEventMethod("ViewOnContactEntryPressed",
                new object[2] {1, phoneNumber});
            yield return new WaitForEndOfFrame();
            Assert.AreEqual(1, View.GetComponentInChildren<ContextMenuView>().transform.GetSiblingIndex());
            yield return new WaitForEndOfFrame();
        }
        
        [UnityTest]
        public IEnumerator When_Contact_Is_Removed_It_Is_Deleted_From_The_View()
        {
            yield return new WaitForEndOfFrame();
            _contact1 = new Contact("contact1", "Foo", "Bar");
            var contactRepo = Main.GetRepository<ContactRepository>();
            contactRepo.AddObject(_contact1);
            yield return new WaitForEndOfFrame();
            Assert.NotNull(Canvas.GetComponentsInChildren<ContactEntry>()
                .FirstOrDefault(entry => entry.PhoneNumber == _contact1.Id));
            contactRepo.RemoveObject(ref _contact1);
            yield return new WaitForEndOfFrame();
            Assert.Null(Canvas.GetComponentsInChildren<ContactEntry>()
                .FirstOrDefault(entry => entry.PhoneNumber == _contact1.Id));
            contactRepo.RemoveObject(ref _contact1);
            contactRepo.Flush();
            yield return new WaitForEndOfFrame();
        }
    }
}