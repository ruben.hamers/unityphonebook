using System.Collections;
using HamerSoft.UnityMvc;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityPhoneBook.Controllers;
using UnityPhoneBook.Models;
using UnityPhoneBook.Repositories;
using UnityPhoneBook.Specs.Mocks;
using UnityPhoneBook.Views;

namespace UnityPhoneBook.Specs
{
    public class DetailsPageControllerTests : AbstractControllerTests<DetailsPageController, DetailsPageView>
    {
        protected override DetailsPageView SetupView(MockCanvas canvas)
        {
            return Controller.SpawnView(canvas.transform);
        }

        [UnityTest]
        public IEnumerator When_Back_Is_Pressed_The_View_Is_Destroyed()
        {
            yield return new WaitForEndOfFrame();
            InvokeControllerViewEventMethod("ViewOnBackPressed", new object[0]);            
            yield return new WaitForEndOfFrame();
            Assert.Catch(() =>
            {
                var test = View.name;
            });
            yield return new WaitForEndOfFrame();
        }

        [UnityTest]
        public IEnumerator When_Back_Is_Pressed_The_ContactsPageView_Is_Shown()
        {
            yield return new WaitForEndOfFrame();
            InvokeControllerViewEventMethod("ViewOnBackPressed", new object[0]);
            yield return new WaitForEndOfFrame();
            Assert.NotNull(Canvas.GetComponentInChildren<ContactsPageView>());
            yield return new WaitForEndOfFrame();
        }

        [UnityTest]
        public IEnumerator When_Delete_Is_Pressed_The_View_Is_Destroyed()
        {
            yield return new WaitForEndOfFrame();
            InvokeControllerViewEventMethod("ViewOnDeletePressed", new object[0]);
            yield return new WaitForEndOfFrame();
            Assert.Catch(() =>
            {
                var test = View.name;
            });
            yield return new WaitForEndOfFrame();
        }

        [UnityTest]
        public IEnumerator When_Delete_Is_Pressed_The_ContactsPageView_Is_Shown()
        {
            yield return new WaitForEndOfFrame();
            InvokeControllerViewEventMethod("ViewOnDeletePressed", new object[0]);
            yield return new WaitForEndOfFrame();
            Assert.NotNull(Canvas.GetComponentInChildren<ContactsPageView>());
            yield return new WaitForEndOfFrame();
        }
        
        [UnityTest]
        public IEnumerator When_Delete_Is_Pressed_The_Contact_With_That_PhoneNumber_Is_Deleted()
        {
            yield return new WaitForEndOfFrame();
            var contactRepo = Main.GetRepository<ContactRepository>();
            string phoneNumber = "ViewOnDeletePressed";
            var contact = new Contact(phoneNumber, "contextmenu", "delete");
            contactRepo.AddObject(contact);
            InvokeControllerViewEventMethod("ViewOnDeletePressed", new object[1] {phoneNumber});
            yield return new WaitForEndOfFrame();
            Assert.Null(contactRepo.GetObject(phoneNumber));
            yield return new WaitForEndOfFrame();
        }
        
        [UnityTest]
        public IEnumerator When_Edit_Is_Pressed_The_View_Is_Destroyed()
        {
            yield return new WaitForEndOfFrame();
            InvokeControllerViewEventMethod("ViewOnEditPressed", new object[0]);
            yield return new WaitForEndOfFrame();
            Assert.Catch(() =>
            {
                var test = View.name;
            });
            yield return new WaitForEndOfFrame();
        }
        
        [UnityTest]
        public IEnumerator When_Edit_Is_Pressed_The_EditContactPageView_Is_Shown()
        {
            yield return new WaitForEndOfFrame();
            var contactRepo = Main.GetRepository<ContactRepository>();
            string phoneNumber = "ViewOnEditPressed";
            var contact = new Contact(phoneNumber, "contextmenu", "edit");
            contactRepo.AddObject(contact);
            InvokeControllerViewEventMethod("ViewOnEditPressed", new object[1]{"ViewOnEditPressed"});
            yield return new WaitForEndOfFrame();
            Assert.NotNull(Canvas.GetComponentInChildren<EditContactPageView>());
            yield return new WaitForEndOfFrame();
            contactRepo.RemoveObject(ref contact);
            contactRepo.Flush();
            yield return new WaitForEndOfFrame();
        }
    }
}