using System.Collections;
using System.Linq;
using NUnit.Framework;
using UnityPhoneBook.Views;
using UnityEngine;
using UnityEngine.TestTools;
using UnityPhoneBook.Repositories;
using UnityPhoneBook.Specs.Mocks;
using UnityPhoneBook.View.Mono;

namespace UnityPhoneBook.Specs
{
    public class AddContactControllerTests : AbstractControllerTests<AddContactController, AddContactView>
    {
        protected override AddContactView SetupView(MockCanvas canvas)
        {
            return Controller.SpawnView(Canvas.transform);
        }

        public override void Setup()
        {
            base.Setup();
            var repo = Main.GetRepository<ContactRepository>();
            var contact1 = repo.GetObject("6565565656");
            var contact2 = repo.GetObject("7878788787");
            if (contact1 != null)
                repo.RemoveObject(ref contact1);
            if (contact2 != null)
                repo.RemoveObject(ref contact2);
            repo.Flush();
        }

        [UnityTest]
        public IEnumerator When_Cancel_Is_Invoked_The_View_Is_Destroyed()
        {
            yield return new WaitForEndOfFrame();
            InvokeControllerViewEventMethod("ViewOnCancelled", new object[0]);
            yield return new WaitForEndOfFrame();
            Assert.Catch(() =>
            {
                var test = View.name;
            });
            yield return new WaitForEndOfFrame();
        }

        [UnityTest]
        public IEnumerator When_Save_Is_Invoked_With_Invalid_Input_The_View_Is_Not_Destroyed()
        {
            yield return new WaitForEndOfFrame();
            InvokeControllerViewEventMethod("ViewOnSaved", new object[0]);
            yield return new WaitForEndOfFrame();
            Assert.NotNull(View);
            yield return new WaitForEndOfFrame();
        }

        [UnityTest]
        public IEnumerator When_Save_Is_Invoked_Valid_Input_The_View_Is_Destroyed()
        {
            yield return new WaitForEndOfFrame();
            InvokeControllerViewEventMethod("ViewOnSaved",
                new object[]
                    {"7878788787", "firstName", "lastName", "description", "email@email.com", "twitterHandle"});
            yield return new WaitForEndOfFrame();
            Assert.Catch(() =>
            {
                var test = View.name;
            });
            yield return new WaitForEndOfFrame();
            var repo = Main.GetRepository<ContactRepository>();
            var contact = repo.GetObject("7878788787");
            repo.RemoveObject(ref contact);
            repo.Flush();
            yield return new WaitForEndOfFrame();
        }

        [UnityTest]
        public IEnumerator When_Save_Is_Invoked_Valid_Input_An_Entry_With_The_Contact_Is_Spawned()
        {
            yield return new WaitForEndOfFrame();
            InvokeControllerViewEventMethod("ViewOnSaved",
                new object[]
                    {"7878788787", "firstName", "lastName", "description", "email@email.com", "twitterHandle"});
            yield return new WaitForEndOfFrame();
            ContactsPageView contactPage = Canvas.GetComponentInChildren<ContactsPageView>();
            yield return new WaitForEndOfFrame();
            Assert.NotNull(contactPage.GetComponentsInChildren<ContactEntry>().ToList()
                .FirstOrDefault(entry => entry.PhoneNumber == "7878788787"));
            var repo = Main.GetRepository<ContactRepository>();
            var contact = repo.GetObject("7878788787");
            repo.RemoveObject(ref contact);
            repo.Flush();
            yield return new WaitForEndOfFrame();
        }
    }
}