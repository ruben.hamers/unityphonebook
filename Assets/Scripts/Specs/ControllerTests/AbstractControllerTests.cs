﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using HamerSoft.Core;
using HamerSoft.UnityMvc;
using NUnit.Framework;
using UnityEngine;
using UnityPhoneBook.Repositories;
using UnityPhoneBook.Specs.Mocks;
using Object = UnityEngine.Object;

namespace UnityPhoneBook.Specs
{
    /// <summary>
    /// simple base controller test class to handle organisational variables and setup
    /// </summary>
    /// <typeparam name="C"></typeparam>
    /// <typeparam name="V"></typeparam>
    public abstract class AbstractControllerTests<C, V> where C : Controller where V : BaseView
    {
        protected C Controller;
        protected V View;
        protected Main Main;
        protected MockCanvas Canvas;

        [SetUp]
        public virtual void Setup()
        {
            GetMain();
            Controller = (C) (object) Main.GetPlugin<MvcPlugin>().GetControllerByView<V>();
            Controller.SetMain(Main);
            Canvas = new GameObject("Root").AddComponent<MockCanvas>();
            View = SetupView(Canvas);
        }

        /// <summary>
        /// Get main from resources
        /// </summary>
        protected void GetMain()
        {
            Main = Resources.Load<Main>("Scriptables/HamerSoftMain");
            Main.Init();
        }

        /// <summary>
        /// setup the view
        /// </summary>
        /// <returns></returns>
        protected abstract V SetupView(MockCanvas canvas);

        [TearDown]
        public virtual void TearDown()
        {
            if (View)
            {
                Object.DestroyImmediate(View.gameObject);
                View = null;
            }
            Main.ApplicationQuit();
        }

        /// <summary>
        /// invoke a controller event, this will intentionally throw an exception when the method is not found to fail the test
        /// </summary>
        /// <param name="name">method name</param>
        /// <param name="args">arguments to the method</param>
        protected void InvokeControllerViewEventMethod(string name, object[] args)
        {
            typeof(C).GetMethod(name, BindingFlags.NonPublic | BindingFlags.Instance)
                .Invoke(Controller, new object[2] {View, args});
        }
    }
}