using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityPhoneBook.Controllers;
using UnityPhoneBook.Models;
using UnityPhoneBook.Repositories;
using UnityPhoneBook.Specs.Mocks;
using UnityPhoneBook.Views;

namespace UnityPhoneBook.Specs
{
    public class ContextMenuControllerTests : AbstractControllerTests<ContextMenuController, ContextMenuView>
    {
        protected override ContextMenuView SetupView(MockCanvas canvas)
        {
            return Controller.SpawnView(canvas.transform);
        }

        public override void Setup()
        {
            base.Setup();
            var contactRepo = Main.GetRepository<ContactRepository>();
            var contact = contactRepo.GetObject("ViewOnDeletePressed");
            if(contact!=null)
                contactRepo.RemoveObject(ref contact);
            contact = contactRepo.GetObject("ViewOnDetailsPressed");
            if(contact!=null)
                contactRepo.RemoveObject(ref contact);
            contactRepo.Flush();
        }

        [UnityTest]
        public IEnumerator When_Delete_Is_Pressed_The_Contact_With_That_PhoneNumber_Is_Deleted()
        {
            yield return new WaitForEndOfFrame();
            var contactRepo = Main.GetRepository<ContactRepository>();
            string phoneNumber = "ViewOnDeletePressed";
            var contact = new Contact(phoneNumber, "contextmenu", "delete");
            contactRepo.AddObject(contact);
            InvokeControllerViewEventMethod("ViewOnDeletePressed", new object[1] {phoneNumber});
            yield return new WaitForEndOfFrame();
            Assert.Null(contactRepo.GetObject(phoneNumber));
            yield return new WaitForEndOfFrame();
        }
        
        [UnityTest]
        public IEnumerator When_Details_Is_Pressed_The_ContactDetailsPage_Is_Shown()
        {
            yield return new WaitForEndOfFrame();
            var contactRepo = Main.GetRepository<ContactRepository>();
            string phoneNumber = "ViewOnDetailsPressed";
            var contact = new Contact(phoneNumber, "contextmenu", "delete");
            contactRepo.AddObject(contact);
            InvokeControllerViewEventMethod("ViewOnDetailsPressed", new object[1] {phoneNumber});
            yield return new WaitForEndOfFrame();
            Assert.NotNull(Canvas.GetComponentInChildren<DetailsPageView>());     
            yield return new WaitForEndOfFrame();
            contactRepo.RemoveObject(ref contact);
            contactRepo.Flush();
            yield return new WaitForEndOfFrame();
        }
    }
}