using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityPhoneBook.Controllers;
using UnityPhoneBook.Models;
using UnityPhoneBook.Repositories;
using UnityPhoneBook.Specs.Mocks;
using UnityPhoneBook.Views;

namespace UnityPhoneBook.Specs
{
    public class EditControllerTests : AbstractControllerTests<EditContactController, EditContactPageView>
    {
        protected override EditContactPageView SetupView(MockCanvas canvas)
        {
            return Controller.SpawnView(canvas.transform);
        }

        [UnityTest]
        public IEnumerator When_Cancel_Is_Pressed_The_EditContactPageView_Is_Destroyed()
        {
            yield return new WaitForEndOfFrame();
            InvokeControllerViewEventMethod("ViewOnCancelPressed", new object[1]{"Testing pressing cancel in the editview!"});
            yield return new WaitForEndOfFrame();
            Assert.Null(Canvas.GetComponentInChildren<EditContactPageView>());
            yield return new WaitForEndOfFrame();
        }
        
        [UnityTest]
        public IEnumerator When_Cancel_Is_Pressed_With_Unknown_PhoneNumber_The_ContactPageView_Is_Shown()
        {
            yield return new WaitForEndOfFrame();
            InvokeControllerViewEventMethod("ViewOnCancelPressed", new object[1]{"very long unknown phonenumber when pressed cancel on editview!"});
            yield return new WaitForEndOfFrame();
            Assert.NotNull(Canvas.GetComponentInChildren<ContactsPageView>());
            yield return new WaitForEndOfFrame();
        }
        
        [UnityTest]
        public IEnumerator When_Cancel_Is_Pressed_With_Known_PhoneNumber_The_DetailsPageView_Is_Shown()
        {
            yield return new WaitForEndOfFrame();
            var contactRepo = Main.GetRepository<ContactRepository>();
            string phoneNumber = "ViewOnCancelPressed";
            var contact = new Contact(phoneNumber, "editview", "cancel");
            contactRepo.AddObject(contact);
            InvokeControllerViewEventMethod("ViewOnCancelPressed", new object[1] {phoneNumber});
            yield return new WaitForEndOfFrame();
            Assert.NotNull(Canvas.GetComponentInChildren<DetailsPageView>());     
            yield return new WaitForEndOfFrame();
            contactRepo.RemoveObject(ref contact);
            contactRepo.Flush();
            yield return new WaitForEndOfFrame();
        }
        [UnityTest]
        public IEnumerator When_Save_Is_Invoked_With_Invalid_Input_The_View_Is_Not_Destroyed()
        {
            yield return new WaitForEndOfFrame();
            InvokeControllerViewEventMethod("ViewOnSavePressed", new object[0]);
            yield return new WaitForEndOfFrame();
            Assert.NotNull(View);
            yield return new WaitForEndOfFrame();
        }
        
        [UnityTest]
        public IEnumerator When_Save_Is_Invoked_Valid_Input_The_Contact_Is_Added_And_DetailsPageView_Is_Shown()
        {
            yield return new WaitForEndOfFrame();
            var repo = Main.GetRepository<ContactRepository>();
            repo.AddObject(new Contact("testing logic for","save","in editview"));
            InvokeControllerViewEventMethod("ViewOnSavePressed",
                new object[]
                    {"testing logic for","1337133713", "firstName", "lastName", "description", "email@email.com", "twitterHandle"});
            yield return new WaitForEndOfFrame();
            Assert.NotNull(Canvas.GetComponentInChildren<DetailsPageView>());
            var contact = repo.GetObject("1337133713");
            Assert.NotNull(contact);
            repo.RemoveObject(ref contact);
            repo.Flush();
            yield return new WaitForEndOfFrame();
        }
    }
}